package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        menu();
    }


    private static void menu() {

        Scanner scan = new Scanner(System.in);
        int key = 0;
        do {
            System.out.println("Для выбора алгоритма нажмите на указанную цифру");
            System.out.println("Алгоритм 1:             1");
            System.out.println("Алгоритм 2:             2");
            System.out.println("Алгоритм 3:             3");
            System.out.println("Выход из программы      4");
            key = scan.nextInt();
            switch (key) {
                case 1:
                    algorithmOne();
                    break;
                case 2:
                    algorithmTwo();
                    break;
                case 3:
                    algorithmThree();
                    break;
                case 4:
                    break;
            }
        } while(key != 4);
    }

    private static void algorithmOne() {
        Scanner scan = new Scanner(System.in);
        int number = 0;
        System.out.print("Введите число: ");
        if (scan.hasNextInt()) {
            number = scan.nextInt();

            if (number > 7) {
                System.out.println("Привет");
            } else {
                System.out.println("Число меньше 7");
            }
        } else {
            System.out.println("Вы ввели не целое число");
        }
    }

    private static void algorithmTwo() {
        String name = "Вячеслав";
        Scanner scan = new Scanner(System.in);
        String nameWrite = "";
        System.out.print("Введите имя: ");

        nameWrite = scan.nextLine();

        if (name.contains(nameWrite)) {
            System.out.println("Привет, Вячеслав");
        } else {
            System.out.println("Нет такого имени");
        }
    }

    private static void algorithmThree() {
        Scanner scan = new Scanner(System.in);
        int sizeMas = 0;
            System.out.print("Введите размер массива: ");
            if (scan.hasNextInt()) {
                sizeMas = scan.nextInt();
                int mas[] = new int[sizeMas];

                for (int i = 0; i < sizeMas; i++) {
                    System.out.print("Элемент " + i + " :");
                    if (scan.hasNextInt()) {
                        mas[i] = scan.nextInt();
                    } else {
                        System.out.println("Вы ввели не целое число");
                        break;
                    }
                }

                System.out.println("Элементы массива кратные 3: ");
                for (int i = 0; i < sizeMas; i++) {
                    if (mas[i] % 3 == 0) {
                        System.out.print(mas[i] + " ");
                    }
                }
                System.out.println();
            } else {
                System.out.println("Вы ввели не целое число");
            }
    }
}
